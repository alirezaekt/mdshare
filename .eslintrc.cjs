module.exports = {
    root: true,
    extends: ['@nuxt/eslint-config'],
    rules: {
        'vue/no-v-html': 'off',
        'vue/multi-word-component-names': 'off',
    },
}

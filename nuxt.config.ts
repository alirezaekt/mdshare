// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ['~/assets/css/global.scss'],
  modules: [
    '@nuxtjs/supabase',
    '@nuxt/ui',
  ],
  supabase: {
    redirect: false,
  },
})

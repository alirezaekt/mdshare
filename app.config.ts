export default defineAppConfig({
    ui: {
        container: {
            constrained: 'max-w-4xl',
        },
        primary: 'green',
        gray: 'neutral',
    }
})
# mdshare
A tool to share your notes e.g. from Obsidian with others with a unique link.

### to do:
- [X] add link preview meta tags
- [X] add theme select and menu 
- [X] add expiry date options for links
- [ ] add writing with prompt practice
- [ ] add unit tests
- [ ] add trigger to clear expired ones in interval
- [ ] adjust row level security and add rate limit for users + nuxt security plugin
- [ ] share md files with drag and drop
- [ ] update eslint [Migrate to v9.x](https://eslint.org/docs/latest/use/migrate-to-9.0.0)

## Setup

add Supabase credentials in .env file. 
```
SUPABASE_URL="url"
SUPABASE_KEY="key"
```

add this table to your Supabase
```
  id text
  created_at timestamptz,
  content json,
  expires_at timestamptz,
  primary key (id)
```

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

[Generating types using Supabase CLI](https://supabase.com/docs/guides/api/rest/generating-types)

// @ts-nocheck
// todo: added above line to ignore the from type issue until it is resolved

import { nanoid } from 'nanoid';

export const useMdShare = () => {
    const supabase = useSupabaseClient();
    const expiryOptions = [
      { label: '1 Hour', value: 1 },
      { label: '1 Day', value: 24 },
      { label: '1 Week', value: 168 },
      { label: '1 Month', value: 720 },
      { label: '1 Year', value: 8760},
      // { label: 'Never', value: null }, // todo: make it only for logged in users
    ];


    const publishMdEntry = async (markdown: { content: string; expiresInHours: number | null }) => {
      if (!markdown.content) {
        throw new Error('Content is required.');
        return;
      }
      const { data, error } = await supabase
        .from('mdshare')
        .insert({
          id: nanoid(8),
          content: {
            md: markdown.content,
          },
          expires_at: markdown.expiresInHours
            ? (new Date(Date.now() + markdown.expiresInHours * 60 * 60 * 1000)).toISOString()
            : null,
        })
        .select();
      if (error) {
        throw new Error('An error occurred while publishing the content.');
        return;
      }
      return (data[0] as { id: string }).id;
    };

    const deleteMdEntry = async (id: string | string[]) => {
      const { error } = await supabase
        .from('mdshare')
        .delete()
        .eq('id', id);
      if (error) {
        console.error(error);
      }
    }

    const fetchMdEntry = async (id: string | string[]) => {
      const { data, error } = await supabase
        .from('mdshare')
        .select('content, expires_at')
        .eq('id', id);
      if (error) {
        console.error(error);
      }
      if (!data || !data[0]) {
        throw new Error('This link does not exist or is expired.');
      }
      if (data[0].expires_at && data[0].expires_at as string < new Date().toISOString()) {
        await deleteMdEntry(id);
        throw new Error('This link does not exist or is expired.');
      }
      return data[0] as { content: { md: string } };
      // modified above until type issue is resolved
    }

    return {
      publishMdEntry,
      fetchMdEntry,
      expiryOptions,
    };
}